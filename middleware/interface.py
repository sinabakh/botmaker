from bot.models import Bot, Client, Category, Post, Membership
import random

class Interface():

    def __init__(self):
        pass

    def get_token_botname(self, token): # token --> get botname related to the token
        try:
            bot = Bot.objects.get(token=token)
        except Bot.DoesNotExist:
            raise IndexError('Bot Does Not Exist!')

        return str(bot.name)

    def get_bot_token(slef, botname): # botname --> token
        try:
            bot = Bot.objects.get(name=botname)
        except Bot.DoesNotExist:
            raise IndexError('Bot Does Not Exist!')

        return str(bot.token)

    def get_cl_state(self, botname, chat_id): # botname,chat_id --> state of client
        try:
            client = Client.objects.filter(bot__name=botname).get(chat_id=chat_id)
        except Client.DoesNotExist:
            raise IndexError('Client Does Not Exist!')

        return str(client.state)

    def get_bot_help(self, botname): # boname --> bot's help message
        try:
            bot = Bot.objects.get(name=botname)
        except Bot.DoesNotExist:
            raise IndexError('Bot Does Not Exist!')

        return str(bot.help)

    def get_bot_des(self, botname): # botname --> bot's description message
        try:
            bot = Bot.objects.get(name=botname)
        except Bot.DoesNotExist:
            raise IndexError('Bot Does Not Exist!')

        return str(bot.description)

    def set_cl_state(self, botname, chat_id, state): # botname,chat_id,state --> save state
        try:
            client = Client.objects.filter(bot__name=botname).get(chat_id=chat_id)
        except Client.DoesNotExist:
            raise IndexError('Client Does Not Exist!')

        try:
            client.state = state
            client.save()
        except:
            raise ValueError('Unknown Error In Saving cl_state!')

    def get_bot_cat(self, botname):
        try:
            cat = Category.objects.filter(bot__name=botname).order_by('pk')
        except Category.DoesNotExist:
            raise IndexError('Category Does Not Exist!')

        return cat.values_list('name', flat=True)

    def get_bot_last_post(self, botname): # botname --> last post of the bot (post.title, post.photo, post.des) {az akhar be aval}
        try:
            post = Post.objects.filter(category__bot__name=botname).order_by('pk').last()
        except Post.DoesNotExist:
            raise IndexError('Post Does Not Exist!')

        return {'title':post.slug, 'text':post.text, 'has_photo_attachment':post.has_photo_attachment, 'photo':post.photo.path}

    def get_cat_last_post(self, botname, cat): # botname, category --> last post of the category in the bot (post.title, post.photo, post.des) {az akhar be aval}
        try:
            post = Post.objects.filter(category__bot__name=botname).filter(category__name=cat).order_by('pk').last()
        except Post.DoesNotExist:
            raise IndexError('Post Does Not Exist!')

        return {'title':post.slug, 'text':post.text, 'has_photo_attachment':post.has_photo_attachment, 'photo':post.photo.path}

    def set_cl_slctd_cat(self, botname, chat_id, cat): # botname, chat_id , category --> save selected category
        try:
            client = Client.objects.filter(bot__name=botname).get(chat_id=chat_id)
        except Client.DoesNotExist:
            raise IndexError('Client Does Not Exist!')

        try:
            client.saved_category = cat
            client.save()
        except:
            raise ValueError('Unknown Error In Saving cl_slctd!')

    def get_cl_slctd_cat(self, botname, chat_id): # botname, category --> get saved category
        try:
            client = Client.objects.filter(bot__name=botname).get(chat_id=chat_id)
        except Client.DoesNotExist:
            raise IndexError('Client Does Not Exist!')

        return str(client.saved_category)

    def register_cl_to_cat(self, botname, cat, chat_id): # botname, cat, chat_id --> register chat_id for the category of botname
        try:
            client = Client.objects.filter(bot__name=botname).get(chat_id=chat_id)
        except Client.DoesNotExist:
            raise IndexError('Client Does Not Exist!')

        try:
            category = Category.objects.filter(bot__name=botname).get(name=cat)
        except Category.DoesNotExist:
            raise IndexError('Category Does Not Exist!')
        try:
            memb = Membership(category=category, client=client)
            memb.save()
        except:
            raise ValueError('Unknown Error In Saving register_cl_to_cat!')


    ##### WARNING: TODO: Make Overriding Membership
    def register_cl_to_bot(self, botname, chat_id): # botname, chat_id --> register chat_id for all category of botname
        try:
            client = Client.objects.filter(bot__name=botname).get(chat_id=chat_id)
        except Client.DoesNotExist:
            raise IndexError('Client Does Not Exist!')

        try:
            categories = Category.objects.filter(bot__name=botname)
        except Category.DoesNotExist:
            raise IndexError('Category Does Not Exist!')
        for category in categories:
            try:
                memb = Membership(category=category, client=client)
                memb.save()
            except:
                raise ValueError('Unknown Error In Saving register_cl_to_cat!')

    def unregister_cl_from_cat(self, botname, cat, chat_id): # botname, cat, chat_id --> unregister chat_id for the category of the botname
        try:
            memb = Membership.objects.filter(client__bot__name=botname).filter(client__chat_id=chat_id).get(cat=cat)
        except Membership.DoesNotExist:
            raise IndexError('Membership Does Not Exist!')

        try:
            memb.delete()
        except:
            raise ValueError('Unknown Error In Unregistering unregister_cl_from_cat!')

    def unregister_cl_from_bot(self, botname, chat_id): # botname, chat_id --> unregister chat_id for all category of botname
        try:
            membs = Membership.objects.filter(client__bot__name=botname).filter(client__chat_id=chat_id)
        except Membership.DoesNotExist:
            raise IndexError('Membership Does Not Exist!')
        for memb in membs:
            try:
                memb.delete()
            except:
                raise ValueError('Unknown Error In Unregistering unregister_cl_from_cat!')

    def get_rand_post_all(self, botname): # botname --> get random post from bot
        try:
            post = random.choice(Post.objects.filter(category__bot__name=botname).all())
        except Post.DoesNotExist:
            raise IndexError('Post Does Not Exist!')

        return {'title':post.slug, 'text':post.text, 'has_photo_attachment':post.has_photo_attachment, 'photo':post.photo.path}

    def get_rand_post_cat(self, botname, cat): # botname, cat --> get random post from the category of bot
        try:
            post = random.choice(Post.objects.filter(category__bot__name=botname).filter(category__name=cat).all())
        except Post.DoesNotExist:
            raise IndexError('Post Does Not Exist!')

        return {'title':post.slug, 'text':post.text, 'has_photo_attachment':post.has_photo_attachment, 'photo':post.photo.path}