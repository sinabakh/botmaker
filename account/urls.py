from django.conf.urls import patterns,url

urlpatterns = patterns(
    'account.views',
    url(r'^signup/$', 'user_sign_up', name='user_sign_up'),
    url(r'^get-auth-token/$', 'user_get_token', name='user_get_token'),
    url(r'^$', 'user_detail', name='user_detail'),
)