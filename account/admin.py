from django.contrib import admin
from .models import User


class UserAdmin(admin.ModelAdmin):
    exclude = ('date_joined', 'last_login', 'password')


admin.site.register(User, UserAdmin)