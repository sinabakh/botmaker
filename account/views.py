from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.exceptions import ParseError
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from account.models import User
from account.serializers import UserSerializer


@api_view(['GET', 'PUT', 'DELETE'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def user_detail(request):
    """
    Get, update, or delete a specific user
    """
    user = request.user

    if request.method == 'GET':
        serializer = UserSerializer(user)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = UserSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def user_get_token(request):
    try:
        data = request.data
    except ParseError as error:
            return Response(
                'Invalid JSON - {0}'.format(error.detail),
                status=status.HTTP_400_BAD_REQUEST
            )
    if "username" not in data or "password" not in data:
            return Response(
                'Bad credentials',
                status=status.HTTP_401_UNAUTHORIZED
            )
    username = request.data['username']
    password = request.data['password']
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        user = None
    if user is None:
        return Response(
                'No such user',
                status=status.HTTP_404_NOT_FOUND
                )
    if user.check_password(password) is False:
        return Response(
                'Wrong credentials',
                status=status.HTTP_401_UNAUTHORIZED
                )
    token = Token.objects.get_or_create(user=user)
    return Response({'detail': 'POST answer', 'token': token[0].key})


@api_view(['POST'])
def user_sign_up(request):
    """
    Create a new user.
    """
    try:
        data = request.data
    except ParseError as error:
            return Response(
                'Invalid JSON - {0}'.format(error.detail),
                status=status.HTTP_400_BAD_REQUEST
            )
    serializer = UserSerializer(data=request.data)
    if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
    else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)