# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0003_auto_20150904_1702'),
    ]

    operations = [
        migrations.AlterField(
            model_name='botownership',
            name='bot',
            field=models.ForeignKey(related_name=b'ownerships', to='bot.Bot'),
        ),
    ]
