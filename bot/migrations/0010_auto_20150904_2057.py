# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0009_auto_20150904_2039'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 4, 20, 57, 44, 799863), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='client',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 4, 20, 57, 56, 680153), auto_now=True),
            preserve_default=False,
        ),
    ]
