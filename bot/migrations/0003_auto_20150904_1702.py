# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0002_auto_20150904_1656'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bot',
            name='expire',
            field=models.DateField(default=datetime.date(2015, 9, 6), blank=True),
        ),
    ]
