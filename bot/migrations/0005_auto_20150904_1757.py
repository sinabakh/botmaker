# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0004_auto_20150904_1748'),
    ]

    operations = [
        migrations.AlterField(
            model_name='botownership',
            name='bot',
            field=models.ForeignKey(to='bot.Bot'),
        ),
    ]
