# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Bot',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=35)),
                ('description', models.CharField(max_length=100)),
                ('bot_username', models.CharField(max_length=40)),
                ('token', models.CharField(max_length=200)),
                ('has_search_command', models.BooleanField(default=False)),
                ('has_getlast_command', models.BooleanField(default=False)),
                ('has_random_command', models.BooleanField(default=True)),
                ('is_active', models.BooleanField(default=False)),
                ('expire', models.DateField()),
                ('image', models.ImageField(default=b'img_folder/None/no-img.jpg', upload_to=b'img_folder/bot_images/')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BotOwnership',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('permission', models.CharField(default=b'AD', max_length=2, choices=[(b'AD', b'Admin'), (b'ED', b'Editor')])),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('bot', models.ForeignKey(to='bot.Bot')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('bot', models.ForeignKey(to='bot.Bot')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('chat_id', models.CharField(max_length=100)),
                ('bot', models.ForeignKey(to='bot.Bot')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
                ('views', models.PositiveIntegerField(default=0)),
                ('has_photo_attachment', models.BooleanField(default=False)),
                ('published', models.BooleanField(default=False)),
                ('photo', models.ImageField(upload_to=b'img_folder/post_images/%Y/%m/%d')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
