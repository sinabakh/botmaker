# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0010_auto_20150904_2057'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='state',
            field=models.CharField(default=b'blank', max_length=25),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bot',
            name='expire',
            field=models.DateField(default=datetime.date(2015, 9, 8), blank=True),
        ),
    ]
