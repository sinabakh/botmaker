# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0013_auto_20150906_2035'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='saved_category',
            field=models.CharField(default=b'blank', max_length=50),
            preserve_default=True,
        ),
    ]
