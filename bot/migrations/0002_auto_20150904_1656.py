# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bot',
            name='description',
            field=models.CharField(max_length=100, blank=True),
        ),
        migrations.AlterField(
            model_name='bot',
            name='expire',
            field=models.DateField(blank=True),
        ),
        migrations.AlterField(
            model_name='bot',
            name='token',
            field=models.CharField(max_length=200, blank=True),
        ),
    ]
