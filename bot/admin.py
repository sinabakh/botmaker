from django.contrib import admin
from .models import Post, Category, Bot, BotOwnership, Client

admin.site.register(Post)
admin.site.register(Category)
admin.site.register(Bot)
admin.site.register(BotOwnership)
admin.site.register(Client)