from django.db import models
from account.models import User
from datetime import date, timedelta


class Bot(models.Model):
    name = models.CharField(max_length=35, blank=False)
    description = models.CharField(max_length=100, blank=True)
    help = models.TextField(blank=False)
    bot_username = models.CharField(max_length=40, blank=False)
    token = models.CharField(max_length=200, blank=True)
    has_search_command = models.BooleanField(default=False)
    has_getlast_command = models.BooleanField(default=False)
    has_random_command = models.BooleanField(default=True)
    is_active = models.BooleanField(default=False)
    expire = models.DateField(blank=True, default=(date.today() + timedelta(days=2)))
    image = models.ImageField(upload_to='img_folder/bot_images/', default='img_folder/None/no-img.jpg')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def is_expired(self):
        return self.expire < date.today()

    def __unicode__(self):
        return self.name


class BotOwnership(models.Model):
    ADMIN = 'AD'
    EDITOR = 'ED'
    PERMISSION_CHOICES = (
        (ADMIN, 'Admin'),
        (EDITOR, 'Editor')
    )
    permission = models.CharField(max_length=2,
                                  choices=PERMISSION_CHOICES,
                                  default=ADMIN)
    user = models.ForeignKey(User)
    bot = models.ForeignKey(Bot)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def is_admin(self):
        return self.permission in (self.ADMIN,)

    def __unicode__(self):
        return str(self.user) + ' --> ' + str(self.bot)


class Client(models.Model):
    bot = models.ForeignKey(Bot)
    chat_id = models.CharField(max_length=100, blank=False)
    state = models.CharField(max_length=25, default='blank')
    saved_category = models.CharField(max_length=50, default='blank')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Category(models.Model):
    name = models.CharField(max_length=50, blank=False)
    bot = models.ForeignKey(Bot)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class Post(models.Model):
    category = models.ForeignKey(Category)
    slug = models.CharField(max_length=35, blank=False)
    text = models.TextField(blank=True)
    views = models.PositiveIntegerField(default=0)
    has_photo_attachment = models.BooleanField(default=False)
    published = models.BooleanField(default=False)
    photo = models.ImageField(upload_to='img_folder/post_images/%Y/%m/%d', null=True, blank=True)

    def has_photo(self):
        return self.has_photo_attachment and self.photo and hasattr(self.photo, 'url')

    def __unicode__(self):
        return self.slug


class Membership(models.Model):
    client = models.ForeignKey(Client)
    category = models.ForeignKey(Category)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
