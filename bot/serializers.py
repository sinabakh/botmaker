from rest_framework import serializers
from bot.models import Bot, BotOwnership, Category, Client, Post
from account.serializers import UserSerializer


class BotSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bot
        fields = ('pk', 'name', 'bot_username', 'description', 'token', 'has_search_command', 'has_getlast_command',
                  'has_random_command', 'is_active', 'expire', 'image', 'created_at',)

    def get_object(self, validated_data):
        return Bot(**validated_data)


class BotSerializerSimple(serializers.ModelSerializer):
    class Meta:
        model = Bot
        fields = ('pk', 'name', 'bot_username', 'description', 'is_active', 'expire',)


class BotOwnershipSerializer(serializers.ModelSerializer):
    bot = BotSerializerSimple()
    user = UserSerializer()

    class Meta:
        model = BotOwnership
        fields = ('pk', 'user', 'permission', 'bot', 'created_at', 'updated_at')


class CategorySerializer(serializers.ModelSerializer):
    bot = BotSerializerSimple()

    class Meta:
        model = Category
        fields = ('pk', 'name', 'bot')


class PostSerializer(serializers.ModelSerializer):
    category = CategorySerializer()

    class Meta:
        model = Post
        fields = ('pk', 'slug', 'category', 'text', 'published', 'views', 'has_photo_attachment', 'photo')


class ClientSerializer(serializers.ModelSerializer):
    bot = BotSerializerSimple()

    class Meta:
        model = Client
        fields = ('pk', 'chat_id', 'bot', 'created_at', 'updated_at')