from django.conf.urls import patterns,url

urlpatterns = patterns(
    'bot.views',
    url(r'^$', 'bot_list', name='bot_list'),
    url(r'^(?P<pk>[0-9]+)/$', 'bot_detail', name='bot_detail'),
    url(r'^(?P<bot_pk>[0-9]+)/owner/$', 'bot_ownership_list', name='bot_ownership_list'),
    url(r'^(?P<bot_pk>[0-9]+)/owner/(?P<ownership_pk>[0-9]+)/$', 'bot_ownership_detail', name='bot_ownership_detail'),
    url(r'^(?P<bot_pk>[0-9]+)/category/$', 'bot_category_list', name='bot_category_list'),
    url(r'^(?P<bot_pk>[0-9]+)/category/(?P<category_pk>[0-9]+)/$', 'bot_category_detail', name='bot_category_detail'),
    url(r'^(?P<bot_pk>[0-9]+)/category/(?P<category_pk>[0-9]+)/post/$', 'bot_category_post_list', name='bot_category_post_list'),
    url(r'^(?P<bot_pk>[0-9]+)/category/(?P<category_pk>[0-9]+)/post/(?P<post_pk>[0-9]+)/$', 'bot_category_post_detail', name='bot_category_post_detail'),
    url(r'^(?P<bot_pk>[0-9]+)/post/$', 'bot_post_list', name='bot_post_list'),
    url(r'^(?P<bot_pk>[0-9]+)/post/(?P<post_pk>[0-9]+)/$', 'bot_post_detail', name='bot_post_detail'),
    url(r'^(?P<bot_pk>[0-9]+)/client/$', 'bot_client_list', name='bot_client_list'),
    url(r'^(?P<bot_pk>[0-9]+)/client/(?P<client_pk>[0-9]+)/$', 'bot_client_detail', name='bot_client_detail'),
)