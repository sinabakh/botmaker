from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.exceptions import ParseError
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import PageNumberPagination

from bot.models import Bot, BotOwnership, Category, Client, Post
from account.models import User
from bot.serializers import BotSerializer, BotSerializerSimple, BotOwnershipSerializer, \
    CategorySerializer, PostSerializer, ClientSerializer

@api_view(['GET', 'POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def bot_list(request):
    """
    List all bots, or create a new bot.
    """
    if request.method == 'GET':
        bots = Bot.objects.filter(botownership__user=request.user)
        paginator = PageNumberPagination()
        result_page = paginator.paginate_queryset(bots, request)
        serializer = BotSerializerSimple(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)

    elif request.method == 'POST':
        try:
            data = request.data
        except ParseError as error:
            return Response(
                'Invalid JSON - {0}'.format(error.detail),
                status=status.HTTP_400_BAD_REQUEST
            )
        serializer = BotSerializer(data=data)
        if serializer.is_valid():
            bot = serializer.save()
            botOwnerShip = BotOwnership(id=None, user=request.user, permission=data['permission'], bot=bot)
            botOwnerShip.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def bot_detail(request, pk):
    """
    Get, update, or delete a specific bot
    """
    try:
        bot = Bot.objects.filter(botownership__user=request.user).get(pk=pk)
    except Bot.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = BotSerializer(bot)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = BotSerializer(bot, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        bot.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def bot_ownership_list(request, bot_pk):
    """
    List all botOwnerships, or create a new botOwnership.
    """
    try:
        bot = Bot.objects.filter(botownership__user=request.user).get(pk=bot_pk)
    except Bot.DoesNotExist:
        return Response(data='No such Bot!', status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        botownerships = BotOwnership.objects.filter(bot=bot)
        paginator = PageNumberPagination()
        result_page = paginator.paginate_queryset(botownerships, request)
        serializer = BotOwnershipSerializer(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)

    elif request.method == 'POST':
        permission = BotOwnership.objects.get(bot=bot, user=request.user).permission
        if permission != 'AD':
            return Response(data='Editors cant create new ownerships.', status=status.HTTP_405_METHOD_NOT_ALLOWED)
        try:
            data = request.data
        except ParseError as error:
            return Response(
                'Invalid JSON - {0}'.format(error.detail),
                status=status.HTTP_400_BAD_REQUEST
            )

        if "username" not in data and "permission" not in data:
            return Response(
                'Wrong info',
                status=status.HTTP_400_BAD_REQUEST
            )
        username = data['username']
        permission = data['permission']
        try:
            user = User.objects.get(username=username)
        except:
            return Response(
                'Wrong username',
                status=status.HTTP_400_BAD_REQUEST
            )
        ownership = BotOwnership(bot=bot, permission=permission, user=user)
        try:
            ownership.save()
            serializer = BotOwnershipSerializer(ownership)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except:
            return Response(data="Unknown Error recurred.", status=status.HTTP_500_INTERNAL_SERVER_ERROR)




@api_view(['GET', 'PUT', 'DELETE'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def bot_ownership_detail(request, bot_pk, ownership_pk):
    """
    Get, update, or delete a specific botOwnership
    """
    try:
        bot = Bot.objects.filter(botownership__user=request.user).get(pk=bot_pk)
    except Bot.DoesNotExist:
        return Response(data='Bot Does Not Exist.', status=status.HTTP_404_NOT_FOUND)

    try:
        botownership = BotOwnership.objects.filter(bot=bot).get(pk=ownership_pk)
    except BotOwnership.DoesNotExist:
        return Response(data='Ownership Does Not Exist.', status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = BotOwnershipSerializer(botownership)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = BotOwnershipSerializer(botownership, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        botownership.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def bot_category_list(request, bot_pk):
    """
    List all categories, or create a new category.
    """
    try:
        bot = Bot.objects.filter(botownership__user=request.user).get(pk=bot_pk)
    except Bot.DoesNotExist:
        return Response(data='No such Bot!', status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        category = Category.objects.filter(bot=bot)
        paginator = PageNumberPagination()
        result_page = paginator.paginate_queryset(category, request)
        serializer = CategorySerializer(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)

    elif request.method == 'POST':
        try:
            data = request.data
        except ParseError as error:
            return Response(
                'Invalid JSON - {0}'.format(error.detail),
                status=status.HTTP_400_BAD_REQUEST
            )
        if "name" not in data:
            return Response(
                'Wrong info',
                status=status.HTTP_400_BAD_REQUEST
            )
        name = data['name']
        category = Category(name=name, bot=bot)
        try:
            category.save()
            serializer = CategorySerializer(category)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except:
            return Response(data="Unknown Error recurred.", status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET', 'PUT', 'DELETE'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def bot_category_detail(request, bot_pk, category_pk):
    """
    Get, update, or delete a specific category
    """
    try:
        bot = Bot.objects.filter(botownership__user=request.user).get(pk=bot_pk)
    except Bot.DoesNotExist:
        return Response(data='Bot Does Not Exist.', status=status.HTTP_404_NOT_FOUND)

    try:
        category = Category.objects.filter(bot=bot).get(pk=category_pk)
    except Category.DoesNotExist:
        return Response(data='Category Does Not Exist.', status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = CategorySerializer(category)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = CategorySerializer(category, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        category.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def bot_category_post_list(request, bot_pk, category_pk):
    """
    List all categories, or create a new post.
    """
    try:
        bot = Bot.objects.filter(botownership__user=request.user).get(pk=bot_pk)
    except Bot.DoesNotExist:
        return Response(data='No such Bot!', status=status.HTTP_404_NOT_FOUND)

    try:
        cat = Category.objects.filter(bot=bot).get(pk=category_pk)
    except Category.DoesNotExist:
        return Response(data='No such Category!', status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        posts = Post.objects.filter(category=cat)
        paginator = PageNumberPagination()
        result_page = paginator.paginate_queryset(posts, request)
        serializer = PostSerializer(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)

    elif request.method == 'POST':
        try:
            data = request.data
        except ParseError as error:
            return Response(
                'Invalid JSON - {0}'.format(error.detail),
                status=status.HTTP_400_BAD_REQUEST
            )
        if "slug" not in data:
            return Response(
                'Wrong info',
                status=status.HTTP_400_BAD_REQUEST
            )
        slug = data['slug']
        post = Post(slug=slug, category=cat)
        try:
            post.save()
            serializer = PostSerializer(post)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except:
            return Response(data="Unknown Error recurred.", status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET', 'PUT', 'DELETE'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def bot_category_post_detail(request, bot_pk, category_pk, post_pk):
    """
    Get, update, or delete a specific post
    """
    try:
        bot = Bot.objects.filter(botownership__user=request.user).get(pk=bot_pk)
    except Bot.DoesNotExist:
        return Response(data='Bot Does Not Exist.', status=status.HTTP_404_NOT_FOUND)

    try:
        category = Category.objects.filter(bot=bot).get(pk=category_pk)
    except Category.DoesNotExist:
        return Response(data='Category Does Not Exist.', status=status.HTTP_404_NOT_FOUND)

    try:
        post = Post.objects.filter(category=category).get(pk=post_pk)
    except Post.DoesNotExist:
        return Response(data='Post Does Not Exist.', status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = PostSerializer(post)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = PostSerializer(post, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        post.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def bot_post_list(request, bot_pk):
    """
    List all posts.
    """
    try:
        bot = Bot.objects.filter(botownership__user=request.user).get(pk=bot_pk)
    except Bot.DoesNotExist:
        return Response(data='No such Bot!', status=status.HTTP_404_NOT_FOUND)


    if request.method == 'GET':
        posts = Post.objects.filter(category__bot=bot)
        paginator = PageNumberPagination()
        result_page = paginator.paginate_queryset(posts, request)
        serializer = PostSerializer(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def bot_post_detail(request, bot_pk, post_pk):
    """
    Get, update, or delete a specific post
    """
    try:
        bot = Bot.objects.filter(botownership__user=request.user).get(pk=bot_pk)
    except Bot.DoesNotExist:
        return Response(data='Bot Does Not Exist.', status=status.HTTP_404_NOT_FOUND)

    try:
        post = Post.objects.filter(category__bot=bot).get(pk=post_pk)
    except Post.DoesNotExist:
        return Response(data='Post Does Not Exist.', status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = PostSerializer(post)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = PostSerializer(post, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        post.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



@api_view(['GET', 'POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def bot_client_list(request, bot_pk):
    """
    List all clients, or create a new client.
    """
    try:
        bot = Bot.objects.filter(botownership__user=request.user).get(pk=bot_pk)
    except Bot.DoesNotExist:
        return Response(data='No such Bot!', status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        clients = Client.objects.filter(bot=bot)
        paginator = PageNumberPagination()
        result_page = paginator.paginate_queryset(clients, request)
        serializer = ClientSerializer(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)

    elif request.method == 'POST':
        try:
            data = request.data
        except ParseError as error:
            return Response(
                'Invalid JSON - {0}'.format(error.detail),
                status=status.HTTP_400_BAD_REQUEST
            )
        if "chat_id" not in data:
            return Response(
                'Wrong info',
                status=status.HTTP_400_BAD_REQUEST
            )
        chat_id = data['chat_id']
        client = Client(chat_id=chat_id, bot=bot)
        try:
            client.save()
            serializer = ClientSerializer(client)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except:
            return Response(data="Unknown Error occurred.", status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET', 'PUT', 'DELETE'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def bot_client_detail(request, bot_pk, client_pk):
    """
    Get, update, or delete a specific category
    """
    try:
        bot = Bot.objects.filter(botownership__user=request.user).get(pk=bot_pk)
    except Bot.DoesNotExist:
        return Response(data='Bot Does Not Exist.', status=status.HTTP_404_NOT_FOUND)

    try:
        client = Client.objects.filter(bot=bot).get(pk=client_pk)
    except Client.DoesNotExist:
        return Response(data='Client Does Not Exist.', status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ClientSerializer(client)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = ClientSerializer(client, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        client.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)