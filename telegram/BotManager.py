# encoding=utf8
import threading
import telegram
import time
import Db

db = Db.Db()


class BotManager():
    @staticmethod
    def set_webhook(self, token, webhook):
        telegram.Bot(token).setWebhook(webhook)

    @staticmethod
    def update(botname, json):
        token = db.get_bot_token(botname)
        bot = telegram.Bot(token)
        update = telegram.Update.de_json(json)
        chat_id = update.message.chat.id
        state = db.get_cl_state(botname, chat_id)

        if state == 'start':  # shoru
            if update.message.text:
                message = update.message.text
                if message == '/help':  # send /help result
                    BotManager.send_message(bot, chat_id, db.get_bot_help(botname))
                elif message == '/description':
                    BotManager.send_message(bot, chat_id, db.get_bot_des(botname))
                elif message == '/search':
                    db.set_cl_state(botname, chat_id, 'search')
                    cats = db.get_bot_cat(botname)
                    reply_markup = telegram.ReplyKeyboardMarkup(cats)
                    BotManager.send_message(bot, chat_id, "لطفا گروه مورد نظر خود را انتخاب کنید", reply_markup=reply_markup)
                elif message == '/last':
                    db.set_cl_state(botname, chat_id, 'last')
                    cats = db.get_bot_cat(botname)
                    reply_markup = telegram.ReplyKeyboardMarkup(cats)
                    BotManager.send_message(bot, chat_id, "لطفا گروه مورد نظر خود را انتخاب کنید", reply_markup=reply_markup)
                elif message == '/register':
                    db.set_cl_state(botname, chat_id, 'register')
                    cats = db.get_bot_cat(botname)
                    reply_markup = telegram.ReplyKeyboardMarkup(cats)
                    BotManager.send_message(bot, chat_id, "لطفا گروه مورد نظر خود را انتخاب کنید", reply_markup=reply_markup)
                elif message == '/random':
                    db.set_cl_state(botname, chat_id, 'random')
                    cats = db.get_bot_cat(botname)
                    reply_markup = telegram.ReplyKeyboardMarkup(cats)
                    BotManager.send_message(bot, chat_id, "لفا گروه مورد نظر خود را انتخاب کنید", reply_markup=reply_markup)
                else:
                    BotManager.send_error(bot, chat_id, 'invalid_command')
            else:
                BotManager.send_error(bot, chat_id, 'only_text')
        elif state == 'random':
            if update.message.text:
                message = update.message.text.encode('utf-8')
                if message == 'برگشت':
                    db.set_cl_state(botname, chat_id, 'start')
                    BotManager.hide_keyboard(bot, chat_id)
                elif message == 'همه':
                    db.set_cl_state(botname, chat_id, 'start')
                    BotManager.hide_keyboard(bot, chat_id)
                    posts = db.get_rand_post_all(botname)
                    BotManager.send_posts(bot, botname, chat_id, posts)
                else:
                    db.set_cl_state(botname, chat_id, 'start')
                    BotManager.hide_keyboard(bot, chat_id)
                    posts = db.get_rand_post_cat(botname, message)
                    BotManager.send_posts(bot, botname, chat_id, posts)
            else:
                BotManager.send_error(bot, chat_id, 'only_text')
        elif state == 'unregister':
            if update.message.text:
                message = update.message.text.encode('utf-8')
                if message == 'برگشت':
                    db.set_cl_state(botname, chat_id, 'start')
                    BotManager.hide_keyboard(bot, chat_id)
                elif message == 'همه':
                    db.set_cl_state(botname, chat_id, 'start')
                    BotManager.hide_keyboard(bot, chat_id)
                    db.unregister_cl_from_bot(botname, chat_id)
                else:
                    db.set_cl_state(botname, chat_id, 'start')
                    BotManager.hide_keyboard(bot, chat_id)
                    db.unregister_cl_from_cat(botname, message, chat_id)
            else:
                BotManager.send_error(bot, chat_id, 'only_text')
        elif state == 'register':
            if update.message.text:
                message = update.message.text.encode('utf-8')
                if message == 'برگشت':
                    db.set_cl_state(botname, chat_id, 'start')
                    BotManager.hide_keyboard(bot, chat_id)
                elif message == 'همه':
                    db.set_cl_state(botname, chat_id, 'start')
                    BotManager.hide_keyboard(bot, chat_id)
                    db.register_cl_to_bot(botname, chat_id)
                else:
                    db.set_cl_state(botname, chat_id, 'start')
                    BotManager.hide_keyboard(bot, chat_id)
                    db.register_cl_to_cat(botname, message, chat_id)
            else:
                BotManager.send_error(bot, chat_id, 'only_text')
        elif state == 'last':  # ask category
            if update.message.text:
                message = update.message.text.encode('utf-8')
                if message == 'برگشت':
                    db.set_cl_state(botname, chat_id, 'start')
                    BotManager.hide_keyboard(bot, chat_id)
                elif message == 'همه':
                    db.set_cl_state(botname, chat_id, 'sending')
                    posts = db.get_bot_last_post(botname)
                    BotManager.hide_keyboard(bot, chat_id)
                    BotManager.send_posts(bot, botname, chat_id, posts)
                else:
                    db.set_cl_state(botname, chat_id, 'sending')
                    posts = db.get_cat_last_post(botname, message)
                    BotManager.hide_keyboard(bot, chat_id)
                    BotManager.send_posts(bot, botname, chat_id, posts)
            else:
                BotManager.send_error(bot, chat_id, 'only_text')
        elif state == 'search':  # ask category
            if update.message.text:
                message = update.message.text.encode('utf-8')
                if message == 'برگشت':
                    db.set_cl_state(botname, chat_id, 'start')
                    BotManager.hide_keyboard(bot, chat_id)
                else:
                    db.set_cl_state(botname, chat_id, 'search-keyword')
                    db.set_cl_slctd_cat(botname, chat_id, message)
                    BotManager.send_message(bot, chat_id, "لطفا کلمه(ها) مورد نظر خود برای جستجو را وارد کنید")
            else:
                BotManager.send_error(bot, chat_id, 'only_text')
        elif state == 'search-keyword':
            if update.message.text:
                db.set_cl_state(botname, chat_id, 'sending')
                message = update.message.text.encode('utf-8')
                posts = db.search_in_bot_cat(botname, db.get_cl_slctd_cat(botname, chat_id), message)
                BotManager.send_posts(bot, botname, chat_id, posts)
            else:
                BotManager.send_error(bot, chat_id, 'only_text')

    @staticmethod
    def send_posts(bot, botname, chat_id, posts):
        threading.Thread(target=BotManager.send_posts_thread, args=(bot, botname, chat_id, posts)).start()

    @staticmethod
    def send_posts_thread(bot, botname, chat_id, posts):
        for post in posts:
            if db.get_cl_state(botname, chat_id) == 'sending':
                BotManager.send_message(bot, chat_id, post.title)
                BotManager.send_photo(bot, chat_id, post.photo)
                BotManager.send_message(bot, chat_id, post.des)
                time.sleep(5)
            else:
                break

    @staticmethod
    def send_photo(bot, chat_id, photo):
        try:
            bot.sendPhoto(chat_id, photo=open(photo, 'rb'))
        except:
            db.unregister_cl_from_bot(bot.token, chat_id)
            db.set_cl_state(db.get_token_botname(bot.token), chat_id, 'start')

    @staticmethod
    def hide_keyboard(bot, chat_id):
        BotManager.send_message(bot, chat_id, '', reply_markup=telegram.ReplyKeyboardHide())

    @staticmethod
    def send_message(bot, chat_id, message, reply_markup=None):
        try:
            bot.sendMessage(chat_id, message, reply_markup)
        except:
            db.unregister_cl_from_bot(bot.token, chat_id)

    @staticmethod
    def send_error(bot, chat_id, error):
        if error == 'only_text':
            BotManager.send_message(bot, chat_id, 'لطفا فقط درخواست متنی ارسال کنید')
        elif error == 'invalid_command':
            BotManager.send_message(bot, chat_id, 'دستور اشتباهی وارد شده است، برای راهنمایی /help را بزنید')

    @staticmethod
    def send_post(bot, botname, cls, post):
        for cl in cls:
            BotManager.send_message(bot, cl, post.title)
            BotManager.send_photo(bot, cl, post.photo)
            BotManager.send_message(bot, cl, post.des)

    @staticmethod
    def send_post_to_cl(botname, cat_id, post_id):
        token = db.get_bot_token(botname)
        bot = telegram.Bot(token)
        cls = db.find_cls_of_cat(botname, cat_id)
        post = db.get_post(post_id)
        BotManager.send_post(bot, botname, cls, post)